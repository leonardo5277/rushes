/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: srabah-m <srabah-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/15 22:04:05 by srabah-m          #+#    #+#             */
/*   Updated: 2013/12/15 22:12:42 by srabah-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <hotrace.h>
#include <string.h>

int	ft_strcmp(const char *s1, const char *s2)
{
	if ((s1 == NULL) || (s2 == NULL))
		return (0);
	while (*s1 == *s2)
	{
		if (*s1 == '\0')
			return (0);
		++s1;
		++s2;
	}
	return ((int)(*s1 - *s2));
}

