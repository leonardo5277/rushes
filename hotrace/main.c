/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: srabah-m <srabah-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/15 21:29:38 by srabah-m          #+#    #+#             */
/*   Updated: 2013/12/15 22:14:08 by srabah-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <get_next_line.h>
#include <hotrace.h>
#include <errno.h>
#include <string.h>

t_item	**g_hash_table;

int		main(void)
{
	if ((init_table()) == -1)
		print_error("Couldn't initialize the hashtable: ");
	if ((get_data() == -1))
		print_error("Couldn't get the input data: ");
	if ((queries()) == -1)
		print_error("Couldn't perform the queries: ");
	free_hash_table();
	return (0);
}

int		get_data(void)
{
	int		ret;

	ret = 1;
	while (ret > 0)
		ret = insert_data();
	if (ret == -1)
		return (-1);
	return (1);
}

int		insert_data(void)
{
	t_item	*item;

	if (!(item = (t_item *)malloc(sizeof(t_item))))
		return (-1);
	if ((item->len_name = (get_next_line(0, &(item->name)))) < 0)
	{
		free(item);
		return (0);
	}
	if (item->name[0] == '\0')
	{
		free(item->name);
		free(item);
		return (0);
	}
	if ((item->len_value = (get_next_line(0, &(item->value)))) < 0)
	{
		free(item->name);
		free(item);
		return (0);
	}
	insert(item);
	return (1);
}

int		queries(void)
{
	t_item	*item;
	char	*query;
	int		ret;

	while ((ret = get_next_line(0, &query)) > 0)
	{
		item = search_item(query);
		if (item == NULL)
		{
			ft_putstr_n(query, ret);
			ft_putstr(": Not found.\n");
		}
		else
			ft_putendl_n(item->value, item->len_value);
		free(query);
	}
	if (ret < 0)
		return (-1);
	return (0);
}

void	print_error(char *error)
{
	ft_putstr(error);
	ft_putendl(strerror(errno));
}

