/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: srabah-m <srabah-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/15 21:25:38 by srabah-m          #+#    #+#             */
/*   Updated: 2013/12/15 22:11:06 by srabah-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <hotrace.h>
#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char const *s)
{
	while (*s)
	{
		write(1, s, 1);
		s++;
	}
}

void	ft_putstr_n(char const *s, int len)
{
	write(1, s, len);
}

void	ft_putendl(char const *s)
{
	ft_putstr(s);
	ft_putchar('\n');
}

void	ft_putendl_n(char const *s, int len)
{
	ft_putstr_n(s, len);
	ft_putchar('\n');
}

