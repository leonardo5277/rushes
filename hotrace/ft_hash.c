/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hash.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: srabah-m <srabah-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/15 21:26:36 by srabah-m          #+#    #+#             */
/*   Updated: 2013/12/15 22:10:32 by srabah-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <hotrace.h>
#include <stdlib.h>

int		hash(char *name)
{
	unsigned int	code;
	unsigned int	tmp;

	code = 0;
	while (*name != 0)
	{
		code = (code << 4) + *name++;
		if ((tmp = (code & 0xf0000000)) != 0)
		{
			code = code ^ (tmp >> 24);
			code = code ^ tmp;
		}
	}
	return (code % SIZE_HTB);
}

void	insert(t_item *new_item)
{
	int		code;
	t_item	*first_item;

	code = hash(new_item->name);
	if (g_hash_table[code] == NULL)
	{
		g_hash_table[code] = new_item;
		new_item->next = NULL;
	}
	else
	{
		first_item = g_hash_table[code];
		g_hash_table[code] = new_item;
		new_item->next = first_item;
	}

}

t_item	*search_item(char *name)
{
	int		code;
	int		finished;
	t_item	*item;

	code = hash(name);
	item = g_hash_table[code];
	finished = 0;
	while (!finished)
	{
		if (item == NULL)
			finished = 1;
		else if (ft_strcmp(item->name, name) == 0)
			finished = 1;
		else
			item = item->next;
	}
	return (item);
}

int		init_table(void)
{
	int	i;

	if (!(g_hash_table = (t_item **)malloc(sizeof(t_item*) * SIZE_HTB)))
		return (-1);
	i = 0;
	while (i < SIZE_HTB)
	{
		g_hash_table[i] = NULL;
		i++;
	}
	return (1);
}

