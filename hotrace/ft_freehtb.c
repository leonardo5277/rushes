/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_freehtb.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: srabah-m <srabah-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/15 21:26:20 by srabah-m          #+#    #+#             */
/*   Updated: 2013/12/15 22:01:35 by srabah-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <hotrace.h>
#include <stdlib.h>

void	free_col_slot(t_item *slot)
{
	if (slot->next != NULL)
		free_col_slot(slot->next);
	free(slot->name);
	free(slot->value);
	free(slot);
}

void	free_hash_table(void)
{
	int	i;

	i = 0;
	while (i < SIZE_HTB)
	{
		if (g_hash_table[i] != NULL)
		{
			free_col_slot(g_hash_table[i]);
		}
		i++;
	}
	free(g_hash_table);
}

