/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hotrace.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: srabah-m <srabah-m@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/15 21:14:11 by srabah-m          #+#    #+#             */
/*   Updated: 2013/12/15 22:18:05 by srabah-m         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef HOTRACE_H
# define HOTRACE_H
# include <string.h>
# define SIZE_HTB 746773

typedef struct		s_item
{
	struct s_item	*next;
	char			*name;
	char			*value;
	int				len_name;
	int				len_value;
}					t_item;
extern t_item		**g_hash_table;

int		init_table(void);
int		hash(char *name);
void	insert(t_item *new_item);
t_item	*search_item(char *name);
void	free_hash_table(void);
void	free_col_slot(t_item *slot);
void	ft_putstr(char const *s);
void	ft_putchar(char c);
void	ft_putendl(char const *s);
void	ft_putstr_n(char const *s, int len);
void	ft_putendl_n(char const *s, int len);
int		queries(void);
int		get_data(void);
int		insert_data(void);
void	print_error(char *error);
int		ft_strcmp(const char *s1, const char *s2);

#endif /* !HOTRACE_H */

